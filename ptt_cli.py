import sys
import ptt_library

if __name__ == "__main__":
    # Verify that we can get the tracking code
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} TRACKINGCODE")
        sys.exit(1)

    # Get the tracking code
    barcode = sys.argv[1]

    print(f"Info for {barcode}:\n")

    # Make the API do a smart query
    barcode_info = ptt_library.do_query(barcode)

    # Go through the events if there's any and print them out
    events = barcode_info["dongu"]
    if events:
        for event in events:
            # This is very ugly, can we improve it?
            detail = event.get("event", event.get("ISLEM")).strip()
            timestamp = event.get(
                "tarih", event.get("ITARIH") + " " + event.get("ISAAT")
            )

            text = f"{detail} on {timestamp}"
            if "ofis" in event:
                text += f" at {event.get('ofis')}"
            elif "IMERK" in event:
                text += f" at {event.get('IMERK')}"
            print(text)
    else:
        print("No data yet.")
