import requests

user_agent = "com.pttxd"
international_url = "https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/yurtDisiKargoSorgulaMSAEHPREMHMRGBAGDOGMAMA"
general_url = "https://pttws.ptt.gov.tr/cepptt/mssnvrPttaceaemaa/gonderitakipvepostakod/gonderisorgu2MSAEHPREMHMRGBAGDOGMAMA"

headers = {"User-Agent": user_agent}


def do_international_query(barcode):
    r = requests.post(international_url, data={"barkod": barcode}, headers=headers)
    return r.json()


def do_national_query(barcode):
    r = requests.post(general_url, data={"barkod": barcode}, headers=headers)
    return r.json()


def do_query(barcode):
    url = international_url if barcode[-2:].upper() == "TR" else general_url
    r = requests.post(url, data={"barkod": barcode}, headers=headers)
    return r.json()
